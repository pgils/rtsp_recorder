#!/bin/sh

set -eu
[ "${DEBUG:-false}" = "true" ] && set -x

gst_launch() {
    gst-launch-1.0 -e \
        rtspsrc \
            protocols=tcp \
            buffer-mode=0 \
            location="${rtsp_source}" \
        ! "rtp${codec}depay" \
        ! queue \
        ! "${codec}parse" \
        ! splitmuxsink \
            muxer=matroskamux \
            location="video%05d.mkv" \
            max-size-time="$(echo "${max_part_length:-60}*1000000000"|bc)" \
    &
    gst_pid=$!
}

# Print disk space used for current work directory in megabytes
disk_space_used() {
    du -sm . | cut -f1
}

# Print disk space available for current work directory in megabytes
disk_space_available() {
    df -Pm . | tail -1 | awk '{print $4}'
}

check_stop_conditions() {
    if test -n "${min_space_available:-}"; then
        if [ "$(disk_space_available)" -le "$min_space_available" ]; then
            print_warn "Minimum disk space limit reached"
            return 1
        fi
    fi

    if test -n "${max_space_used:-}"; then
        if [ "$(disk_space_used)" -ge "$max_space_used" ]; then
            print_warn "Maximum disk space limit reached"
            return 1
        fi
    fi

    if test -n "${max_time:-}"; then
        if [ "$(date +%s)" -ge "$time_record_stop" ]; then
            print_warn "Time limit reached"
            return 1
        fi
    fi

    return 0
}

print_info() {
    printf "[\033[1;35m+\033[0m] INFO: %s\n" "${1}"
}

print_warn() {
    printf "[\033[1;33m!\033[0m] \033[1;33mWARNING\033[0m: %s\n" "${1}" >&2
}

print_usage() {
    cat <<HEREDOC
  usage: $(basename "${0}") [OPTIONS] RTSP_SOURCE
    options
      -p, --part-length     Video part length in seconds. (Default: 60)
      -a, --min-available   Minimum disk space to leave available in MB.
      -u, --max-used        Maximum disk space for recording in MB.
      -t, --time            Record time in minutes.
      -c, --codec           Video codec used by the input stream. (Default: h264)
HEREDOC
}

while true; do

    case "${1:-}" in
        -h|--help)
            print_usage
            exit 0
            ;;
        -p|--part-length)
            max_part_length="$2"
            print_info "Video will be split into parts  max $max_part_length seconds"
            ;;
        -a|--min-available)
            min_space_available="$2"
            print_info "Current available disk space is $(disk_space_available) MB"
            print_info "Recording will stop if          $min_space_available MB is reached"
            ;;
        -u|--max-used)
            max_space_used="$2"
            print_info "Current disk space used is      $(disk_space_used) MB"
            print_info "Recording will stop if          $max_space_used MB is reached"
            ;;
        -t|--time)
            max_time="$2"
            time_record_start=$(date +%s)
            time_record_stop=$(echo "$time_record_start + ($max_time*60)"|bc)
            print_info "The current time is             $(date)"
            print_info "Recording will stop at          $(date -d@"$time_record_stop")"
            ;;
        -c|--codec)
            codec="$2"
            ;;
        -?*)
            echo "Unknown option '$1'"
            print_usage
            exit 1
            ;;
        *) break ;;
    esac

    shift 2
done

rtsp_source="${1:?RTSP source missing}"
shift

: "${codec:=h264}"
print_info "Using video encoding            $codec"

[ "$#" -gt 0 ] && print_warn "Ignoring trailing arguments: $*"

if [ -z "${min_space_available:-}" ] && [ -z "${max_space_used:-}" ] && [ -z "${max_time:-}" ]; then
    print_warn "Recording indefinitely. Consider using a limiter (see --help)"
fi

# Main loop
gst_launch

while true; do
    check_stop_conditions || break
    sleep 10
done

kill -SIGTERM "$gst_pid"
