FROM alpine:3.17

# hadolint ignore=DL3018
RUN apk add --no-cache \
    gstreamer-tools \
    gst-plugins-good \
    gst-plugins-bad \
    aws-cli

# Optional

# credentials for manual uploading with AWS cli
ENV AWS_REGION=
ENV AWS_ACCESS_KEY_ID=
ENV AWS_SECRET_ACCESS_KEY=

# Output for video files. Should be mapped to some volume.
VOLUME /video

COPY record.sh /usr/local/bin/record

WORKDIR /video
ENTRYPOINT ["record"]
